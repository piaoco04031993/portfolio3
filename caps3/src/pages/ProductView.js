import {useState, useEffect} from 'react'
import Banner from './../components/Banner';

import {Row, Col, Card, Button, Container} from 'react-bootstrap';

import {/*Link, */useParams} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function ProductView() {
	
	const [productInfo, setProductInfo] = useState({
			name: null,
			description: null,
			price: null
		});

const data = {
	title:`${productInfo.name}`,
	content:''
}

	console.log(useParams())

	const {id} = useParams()
	console.log(id)

	useEffect(() => {

		fetch(`https://whispering-gorge-43888.herokuapp.com/products/${id}`).then(res => res.json()).then(convertedData => {
			console.log(convertedData)

			setProductInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		})
	},[id])

	const order = () => {
		return (
				Swal.fire({
					icon: 'success',
					title:'Sucessful!!',
					text:'Successfully Added to Cart'
				})
			)
	};

	return(
		<>
			<Banner bannerData={data} />
			<Row className="mx-5">
				<Col md={6} sm={12} className="container-fluid">
					<Card>
						{<Card.Img variant="top" className="productimage" src={`/${productInfo.name}.jpg`} />}
					</Card>
				</Col>
				<Col md={5} sm={12} className="ml-2">
					<Row>
						<Col>
							<Container>
								<Card className="text-center details">
									<Card.Body>
										{/*Product Name*/}
										<Card.Title>
											<h3>{productInfo.name}</h3>
										</Card.Title>
										
										{/*Product Desscription*/}
										<Card.Subtitle>
											<h6 className="my-4">Description: </h6>
										</Card.Subtitle>
										<Card.Text>
											{productInfo.description}
										</Card.Text>
										{/*Product Price*/}
										<Card.Subtitle className="my-4">
											<h6>Price:  </h6>
										</Card.Subtitle>
										<Card.Text>
											₱ {productInfo.price}
										</Card.Text>
									</Card.Body>
									<Row>
									<Col className="ml-auto">
									<Button variant="secondary" className="" onClick={order}>
										Add to Cart
									</Button>
										
									</Col>
									<Col className="mr-auto">
									<Button variant="danger" className="" onClick={order}>
										Add to Wishlist
									</Button>
										
									</Col>
									</Row>
									{/*<Link className="btn btn-success btn-block mb-5" to="/login">
										Login to Order
									</Link>*/}
								</Card>
							</Container>
						</Col>
					</Row>
				</Col>
			</Row>		
				
			
		</>

			);
}