import {Container} from 'react-bootstrap'
import Hero from './../components/Banner';
import ProductCard from './../components/ProductCard';
import { useState, useEffect } from 'react';
const bannerDetails = {
	title: 'Products',
	content: 'Featured Products Base on Your Search'
}
export default function Products () {
	const [productsCollection, setProductCollection] = useState([]);
	useEffect(() => {
		fetch('https://whispering-gorge-43888.herokuapp.com/products/').then(res => res.json()).then(convertedData => {
			setProductCollection(convertedData.map(product => {
				return(
					<ProductCard key={product._id} productProp={product}/>
					)
			}))
		})
	}, [])
	return (
		<>
			<Hero bannerData={bannerDetails}/>
			<Container className="text-center">
				{productsCollection}
			</Container>
		</>
		);
}