import {Row, Col} from 'react-bootstrap';

export default function About () {
	return(
		
		<Row className="aboutMe">
		<Col xs={12} md={3} className="my-4 text-center">
		<img className="w-75 h-75" src='./me.jpg'/>
		</Col>
			<Col xs={12} md={9} className="text-white">
				<h3 className="my-3">Title: ABOUT ME</h3>
				<h4>Pia O. Ogawa</h4>
				<h5>Full Stack Web Developer</h5>
				<p>I'm an aspiring software engineer who is currently studying in Zuitt Coding Bootcamp</p>
				<h5>Contact Me:</h5>
				<ul>
					<li>Email: piaoco04031993@gmail.com</li>
					<li>Mobile No: 09203186423</li>
					<li>Address: Daet, Camarines Norte</li>
				</ul>
			</Col>
		</Row>
		
		);
}
