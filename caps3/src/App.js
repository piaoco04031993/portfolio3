
import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Products from './pages/Products'
// import AllProducts from './pages/AllProducts'
import Error  from './pages/Error'
import ProductView  from './pages/ProductView'
import AddProduct  from './pages/AddProduct'
import './App.css';
import { UserProvider } from './UserContext';

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    });
  }

  useEffect(() => {
    let token = localStorage.getItem('access');
    fetch('https://thawing-sea-49056.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => res.json()).then(convertedData => {
      if (typeof convertedData._id !== 'undefined') {
        setUser({
          id: convertedData._id,
          isAdmin: convertedData.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      }
    });
  },[user]);

  return (
    <div className = "bg">
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavBar />
          <Routes>
            <Route exact path='/' element={<Home />} />
            <Route path='/register' element={<Register />} />
            <Route path='/products' element={<Products />}></Route>
            {/*<Route path='/products/all' element={<AllProducts />}></Route>*/}
            <Route path='/products/create' element={<AddProduct />}></Route>
            <Route path='*' element={<Error />}></Route>
            <Route path='/login' element={<Login />}></Route>
            <Route path='/logout' element={<Logout />}></Route>
            <Route path='/products/view/:id' element={<ProductView />}></Route>
          </Routes>
        </Router>
      </UserProvider>   
    </div>
  );
}

export default App;
